    <h1 class="page-header">Datos de la Persona</h1>

    <div class="well well-sm text-right">
        <a class="btn btn-primary" href="?c=persona&a=Nuevo"></a>
        <a class="btn btn-primary" href="?c=colegio&a=Nuevo"></a>
    </div>
    
    <table class="table table-striped">
        <thead>
            <tr>
                <th style="width:180px;">Documento</th>
                <th style="width:180px;">Nombre</th>
                <th style="width:120px;">Apellido</th>
                <th style="width:120px;">Generos</th>
                <th style="width:120px;">Fecha Examen</th>
                <th style="width:120px;">Tipo de Examen</th>
                <th style="width:120px;">Condicion</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach($result as $r): ?>
            <tr>
                <td><?php echo $r->dni_number; ?></td>
                <td><?php echo $r->name; ?></td>
                <td><?php echo $r->surname; ?></td>
                <td><?php echo $r->gender; ?></td>
                <td><?php echo $r->date_time; ?></td>
                <td><?php echo $r->type_test; ?></td>
                <td><?php echo $r->detail; ?></td>
                <td>
                    <a href="?c=persona&a=Crud&Documento=<?php echo $r->dni_number; ?>">Editar</a>
                </td>
                <td>
                    <a onclick="javascript:return confirm('¿Seguro de eliminar este registro?');" href="?c=persona&a=Eliminar&Documento=<?php echo $r->Documento; ?>">Eliminar</a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
