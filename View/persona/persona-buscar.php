<h1 class="page-header">
    Busqueda de Personas
</h1>

<ol class="breadcrumb">
  <li><a href="?c=persona">Listado</a></li>
  <li class="active">Busqueda de Personas</li>
</ol>

<form id="frm-persona" action="?c=persona&a=Buscar" method="post" enctype="multipart/form-data">

    <div class="form-group">
      <label>Documento</label>
      <input type="text" name="dni_number" value="<?php echo $pvd->dni_number; ?>" class="form-control" placeholder="Ingrese Documento del la Persona" data-validacion-tipo="requerido|min:20" />
    </div>

    <hr />

    <div class="text-right">
        <button class="btn btn-success">Buscar</button>
    </div>
</form>

<script>
    $(document).ready(function(){
        $("#frm-persona").submit(function(){
            return $(this).validate();
        });
    })
</script>
