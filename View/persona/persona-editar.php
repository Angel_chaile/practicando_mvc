<h1 class="page-header">
    <?php echo $pvd->dni_number != null ? $pvd->surname : 'Datos de la Persona'; ?>
</h1>

<ol class="breadcrumb">
  <li><a href="?c=persona">Listado</a></li>
  <li class="active"><?php echo $pvd->dni_number != null ? $pvd->surname : 'Editar Fecha'; ?></li>
</ol>

<form id="frm-persona" action="?c=persona&a=Editar" method="post" enctype="multipart/form-data">
    <input type="hidden" name="dni_number" value="<?php echo $pvd->dni_number; ?>" />

    <div class="form-group">
        <label>Nombre</label>
        <input type="text" name="name" value="<?php echo $pvd->name; ?>" class="form-control" placeholder="Ingrese Nombre de la persona" data-validacion-tipo="requerido|min:100" />
    </div>
    
    <div class="form-group">
         <label>Apellido</label>
         <input type="text" name="surname" value="<?php echo $pvd->surname; ?>" class="form-control" placeholder="Ingrese Apellido de la persona" data-validacion-tipo="requerido|min:100" />
     </div>

    <div class="form-group">
        <label>Fecha de Examen</label>
        <input type="date" name="date_time" value="<?php echo $pvd->$date_time; ?>" class="form-control" placeholder="Ingrese la fecha del examen" data-validacion-tipo="requerido|min:10" />
    </div>

    <hr />

    <div class="text-right">
        <button class="btn btn-success">Actualizar</button>
    </div>
</form>

<script>
    $(document).ready(function(){
        $("#frm-persona").submit(function(){
            return $(this).validate();
        });
    })
</script>
