<?php
//Se incluye el modelo donde conectará el controlador.
require_once 'Model/persona.php';

class PersonaController{

    private $model;

    //Creación del modelo
    public function __CONSTRUCT(){
        $this->model = new persona();
    }

    //Llamado plantilla principal
    public function Index(){
        require_once 'View/header.php';
        require_once 'View/persona/persona.php';
        require_once 'View/footer.php';
    }

    //Llamado a la vista persona-editar
    public function Crud(){
        $pvd = new persona();
            
        //Se obtienen los datos de la persona a editar.
        if(isset($_REQUEST['dni_number'])){
            
            $pvd = $this->model->Obtener($_REQUEST['dni_number']);
            
        }
        
        //Llamado de las vistas.
        require_once 'View/header.php';
        require_once 'View/persona/persona-editar.php';
        require_once 'View/footer.php';
    }

    //Llamado a la vista persona-buscar
    public function Busqueda(){
        $pvd = new persona();

        //Llamado de las vistas.
        require_once 'View/header.php';
        require_once 'View/persona/persona-buscar.php';
        require_once 'View/footer.php';
    }

    //Método que registrar al modelo un nuevo votante.
    public function Guardar(){
        $pvd = new persona();

        //Captura de los datos del formulario (vista).
        $pvd->Documento = $_REQUEST['Documento'];
       /* $pvd->Apellido = $_REQUEST['Apellido'];
        $pvd->Nombre = $_REQUEST['Nombre'];
        $pvd->Mesa = $_REQUEST['Mesa'];
        $pvd->Domicilio = $_REQUEST['Domicilio'];*/


        //Registro al modelo votante.
        $this->model->Registrar($pvd);

        //header() es usado para enviar encabezados HTTP sin formato.
        //"Location:" No solamente envía el encabezado al navegador, sino que
        //también devuelve el código de status (302) REDIRECT al
        //navegador
        header('Location: index.php');
    }

    //Método que modifica el modelo de una persona.
    public function Editar(){
        $pvd = new persona();

        $pvd->dni_number = $_REQUEST['dni_number'];
        $pvd->surname = $_REQUEST['surname'];
        $pvd->name = $_REQUEST['name'];
        $pvd->date_time = $_REQUEST['date_time'];

        $this->model->Actualizar($pvd);

        header('Location: index.php');
    }

    //Método que elimina la tupla votante con el documento dado.
    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['Documento']);
        header('Location: index.php');
    }

    public function Buscar(){
        $result = $this->model->Buscar($_POST['dni_number']);
        
        require_once 'View/header.php';
        require_once 'View/persona/ResultadoBusqueda.php';
        require_once 'View/footer.php';
    }
}
