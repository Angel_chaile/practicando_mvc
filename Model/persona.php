<?php
class persona
{
	//Atributo para conexión a SGBD
	private $pdo;

		//Atributos del objeto persona
    public $dni_number;
    public $name;
    public $surname;
    public $id_test;
	public $date_time;
	public $id_status;

	//Método de conexión a SGBD.
	public function __CONSTRUCT()
	{
		try
		{
			$this->pdo = Database::Conectar();
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	//Este método selecciona todas las tuplas de la tabla
	//Person en caso de error se muestra por pantalla.
	public function Listar()
	{
		try
		{
			$result = array();
			//Sentencia SQL para selección de datos.
			$stm = $this->pdo->prepare("SELECT dni_number, name, surname, gender, date_time, type_test, detail FROM `Person` 
			JOIN Genders on Person.id_gender = Genders.id_gender
			JOIN Status on Person.id_status = Status.id_status
			JOIN Test on Person.id_test = Test.id_test JOIN Type_Test on Test.id_type_test = Type_Test.id_type_test");
			//WHERE DAY(date_time) = DAY(NOW())");
			//Ejecución de la sentencia SQL.
			$stm->execute();
			//fetchAll — Devuelve un array que contiene todas las filas del conjunto
			//de resultados
			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			//Obtener mensaje de error.
			die($e->getMessage());
		}
	}



	public function Buscar($dni_number)
	{
		try
		{
			//Sentencia SQL para selección de datos.
			$stm = $this->pdo->prepare("SELECT dni_number, name, surname, gender, date_time, type_test, detail FROM `Person` 
			JOIN Genders on Person.id_gender = Genders.id_gender
			JOIN Status on Person.id_status = Status.id_status
			JOIN Test on Person.id_test = Test.id_test JOIN Type_Test on Test.id_type_test = Type_Test.id_type_test
			WHERE Person.dni_number = ?");
			//Ejecución de la sentencia SQL.
			$stm->execute(array($dni_number));
			//fetchAll — Devuelve un array que contiene todas las filas del conjunto
			//de resultados
			return $stm->fetchAll(PDO::FETCH_OBJ);
	
		}
		catch(Exception $e)
		{
			//Obtener mensaje de error.
			die($e->getMessage());
		}
	}

	//Este método obtiene los datos de la persona a partir del documento
	//utilizando SQL.
	public function Obtener($dni_number)
	{
		try
		{
			//Sentencia SQL para selección de datos utilizando
			//la clausula Where para especificar el documento de la persona.
			$stm = $this->pdo->prepare("SELECT dni_number, name, surname, date_time FROM Person WHERE dni_number = ?");
			//Ejecución de la sentencia SQL utilizando el parámetro documento.
			$stm->execute(array($dni_number));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e)
		{
			die($e->getMessage());
		}
	}

	//Este método elimina la tupla votante dado un Documento.
	public function Eliminar($Documento)
	{

		try
		{
			//Sentencia SQL para eliminar una tupla utilizando
			//la clausula Where.
			$stm = $this->pdo
			            ->prepare("DELETE FROM Padronsn WHERE Documento = ? and nombre = ? and apellido = ?");

			$stm->execute(array($Documento, $nombre, $apellido));
		} catch (Exception $e)
		{
			die($e->getMessage());
		}
	}

	//Método que actualiza una tupla a partir de la clausula
	//Where y el Documento del persona.
	public function Actualizar($data)
	{
		try
		{
			//Sentencia SQL para actualizar los datos.
			$sql = "UPDATE Person SET
						name         = ?,
						surname      = ?,
            			date_time    = ?
				    WHERE dni_number = ?";
			//Ejecución de la sentencia a partir de un arreglo.
			$this->pdo->prepare($sql)
			     ->execute(
				    array(
                        $data->name,
                        $data->surname,
                        $data->date_time,
                        $data->dni_number
					)
				);
		} catch (Exception $e)
		{
			die($e->getMessage());
		}
	}

	//Método que registra un nuevo votante a la tabla.
	public function Registrar(persona $data)
	{
		try
		{
			//Sentencia SQL.
			$sql = "INSERT INTO Padronsn (Documento,Apellido,Nombre,Mesa,Domicilio)
		        VALUES (?, ?, ?, ?, ?)";

			$this->pdo->prepare($sql)
		     ->execute(
				array(
                    $data->Documento,
                    $data->Apellido,
                    $data->Nombre,
                    $data->Mesa,
					$data->Domicilio,
                )
			);
		} catch (Exception $e)
		{
			die($e->getMessage());
		}
	}
}
